package com.ex.app.services;


import com.ex.app.models.User;
import com.ex.app.persistence.Persistable;
import com.ex.app.persistence.SqlBankPersistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Scanner;

public class BankService {
    Persistable<User, Integer> bankPersistence;

        public BankService(){

        }

        public void setBankPersistence(Persistable<User, Integer> bankPersistence){
            this.bankPersistence = bankPersistence;
        }

        public List<User> findAllUsers(){
            return bankPersistence.getAll();
        }

        public Integer addUser(User input) {
            return bankPersistence.save(input);
        }

        public User getUser(int idNum){
            return bankPersistence.getById(idNum);
        }


    }


