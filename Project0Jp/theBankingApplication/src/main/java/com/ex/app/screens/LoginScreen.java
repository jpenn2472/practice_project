package com.ex.app.screens;

import com.ex.Application;
import com.ex.Screen;
import com.ex.app.persistence.SqlBankPersistence;
import com.ex.app.services.BankService;
import com.ex.app.system.BankApplication;

import java.util.Scanner;

public class LoginScreen implements Screen {
    SqlBankPersistence sqlb = new SqlBankPersistence();

    private BankService bankService;
    Screen menuScreen = new WelcomeScreen();

    public Screen doScreen(Scanner scanner, Application app) {
        bankService = (BankService) ((BankApplication) app).getAppContext().get("bankService");
        Scanner scan = new Scanner(System.in);
        System.out.println("Are you an employee or a customer?");
        String acctType = scan.nextLine();

        if (acctType.equalsIgnoreCase("customer") ) {
            try {

                sqlb.validateLogin("customer", false);
                Scanner scann = new Scanner(System.in);
                System.out.println("Enter 1 to continue to customer menu.");
                int ans = scann.nextInt();
                if (ans == 1) {
                    Screen nextScreen = new WelcomeScreen();
                    return nextScreen;

                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        else if (acctType.equalsIgnoreCase("employee")) {

        try {
            sqlb.validateLogin("employee", true);
            Scanner scann = new Scanner(System.in);
            System.out.println("Enter 1 to continue to employee menu.");
            int anss = scann.nextInt();
            if (anss == 1) {
                Screen nextScreen = new EmployeeWelcomeScreen();
                return nextScreen;

            }
        }
          catch(Exception ex){

            }
        }
        return null;
    }
}