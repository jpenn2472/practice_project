package com.ex.app.models;

public class User {
    private int id;
    private String fName;
    private int balanceAmt;
    private String lName;
    private String goodStanding;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public int getBalanceAmt() {
        return balanceAmt;
    }

    public void setBalanceAmt(int balanceAmt) {
        this.balanceAmt = balanceAmt;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getGoodStanding() {
        return goodStanding;
    }

    public void setGoodStanding(String goodStanding) {
        this.goodStanding = goodStanding;
    }

    public User(int id, String fName, int balanceAmt, String lName, String goodStanding) {
        this.id = id;
        this.fName = fName;
        this.balanceAmt = balanceAmt;
        this.lName = lName;
        this.goodStanding = goodStanding;
    }

    @Override
    public String toString() {
        return "users{" +
                "id=" + id +
                ", title='" + fName + '\'' +
                ", reviewRating=" + balanceAmt +
                ", esrbRating='" + lName + '\'' +
                ", publisher='" + goodStanding + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                balanceAmt == user.balanceAmt &&
                fName.equals(user.fName) &&
                lName.equals(user.lName) &&
                goodStanding.equals(user.goodStanding);
    }
}

