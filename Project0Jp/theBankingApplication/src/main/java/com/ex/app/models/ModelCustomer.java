package com.ex.app.models;

public class ModelCustomer {

    private int customerId;
    private String fName;
    private String lName;
    private int creditScore;
    private int acctNum;

    public ModelCustomer() {

    }
    public ModelCustomer(int customerId, String fName, String lName, int creditScore, int acctNum) {
        super();
        this.customerId = customerId;
        this.fName = fName;
        this.lName = lName;
        this.creditScore = creditScore;
        this.acctNum = acctNum;
    }

    public int getAcctNum() {
        return acctNum;
    }

    public void setAcctNum(int acctNum) {
        this.acctNum = acctNum;
    }

    public int getId() {
        return customerId;
    }

    public void setId(int id) {
        this.customerId = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getCreditScore() {
        return creditScore;
    }

    public void setCreditScore(int creditScore) {
        this.creditScore = creditScore;
    }

    @Override
    public String toString() {
        return customerId + " " + fName + " " + lName + " " + creditScore;
    }


}