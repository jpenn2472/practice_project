package com.ex.app.persistence;

import java.util.List;

public interface Persistable<T, ID> {
    T getById(ID id);
    List<T> getAll();
    ID save(T obj);
    void update(T obj);
    void delete(T obj);
}
