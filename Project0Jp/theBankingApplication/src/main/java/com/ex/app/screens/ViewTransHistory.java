package com.ex.app.screens;

import com.ex.Application;
import com.ex.Screen;
import com.ex.app.models.User;
import com.ex.app.services.BankService;
import com.ex.app.system.BankApplication;

import java.util.List;
import java.util.Scanner;

public class ViewTransHistory implements Screen {
    private BankService bankService;
    public Screen doScreen(Scanner scanner, Application app) {
        bankService = (BankService) ((BankApplication)app).getAppContext().get("bankService");
        System.out.println("-------- Browse ---------");
        List<User> users = bankService.findAllUsers();

        if(users == null || users.isEmpty()) {
            System.out.println("No games to browse");
        } else {
            for(int i = 0; i < users.size(); i++) {
                System.out.println(String.format("%d. %s\n", i +1, users.get(i).getfName()));
            }
        }
        return null;
    }
}
