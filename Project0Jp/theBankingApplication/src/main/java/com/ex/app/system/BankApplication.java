package com.ex.app.system;

import com.ex.Application;
import com.ex.Screen;
import com.ex.app.models.User;
import com.ex.app.persistence.Persistable;
import com.ex.app.persistence.SqlBankPersistence;
import com.ex.app.screens.LoginScreen;
import com.ex.app.services.BankService;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

;
//import com.ex.app.screens.LoginScreen;

public class BankApplication extends Application {
    private Screen currentScreen;
    private Scanner scanner;
    private Map<String, Object> appContext;

    public BankApplication() {}

    public BankApplication(String title) {
        this.title = title;
        //this was new WelcomeScreen();
        this.currentScreen = new LoginScreen();
        this.scanner = new Scanner(System.in);
    }

    @Override
    public void run(String[] args) throws Exception {
        init();
        while(currentScreen != null) {
            currentScreen = currentScreen.doScreen(scanner, this);
        }

    }

    public Map<String, Object> getAppContext() {
        return appContext;
    }

    private void init() throws Exception {
        appContext = new HashMap();
        Persistable<User, Integer> bankPersistence = new SqlBankPersistence();
        BankService bankService = new BankService();
        bankService.setBankPersistence(bankPersistence);

        appContext.put("bankService", bankService);
    }
}
