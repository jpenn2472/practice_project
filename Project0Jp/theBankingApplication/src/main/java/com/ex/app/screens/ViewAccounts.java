package com.ex.app.screens;

import com.ex.Application;
import com.ex.Screen;
import com.ex.app.persistence.SqlBankPersistence;
import com.ex.app.services.BankService;
import com.ex.app.system.BankApplication;

import java.util.Scanner;

public class ViewAccounts implements Screen {
    SqlBankPersistence sqlb = new SqlBankPersistence();

    private BankService bankService;
    Screen menuScreen = new WelcomeScreen();

    public Screen doScreen(Scanner scanner, Application app) {
        bankService = (BankService) ((BankApplication)app).getAppContext().get("bankService");


        try {
            sqlb.viewAccounts();
        } catch (Exception ex) {

        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 1 to continue to customer main menu.");
        int ans = scan.nextInt();
        if (ans == 1) {
            Screen nextScreen = new WelcomeScreen();
            return nextScreen;

        }return null;
    }
}
