package com.ex.app.screens;

import com.ex.Application;
import com.ex.Screen;
import com.ex.app.persistence.SqlBankPersistence;
import com.ex.app.services.BankService;

import java.util.Scanner;

public class WithdrawScreen implements Screen {
    private BankService bankService;
    public Screen doScreen(Scanner scanner, Application app) {
        SqlBankPersistence stmt = new SqlBankPersistence();
        //bankService = (BankService) ((BankApplication)app).getAppContext().get("bankService");
        //List<User> users = bankService.findAllUsers();
        try {
            stmt.withdrawOut();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 1 to continue to main menu");
        int ans = scan.nextInt();
        if (ans == 1) {
            Screen nextScreen = new WelcomeScreen();
            return nextScreen;

        }
        return null;
    }
}
