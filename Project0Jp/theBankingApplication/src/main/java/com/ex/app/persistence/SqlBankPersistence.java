package com.ex.app.persistence;

import com.ex.Screen;
import com.ex.app.models.ModelCustomer;
import com.ex.app.models.ModelEmployee;
import com.ex.app.models.User;
import com.ex.app.screens.EmployeeWelcomeScreen;

import java.sql.*;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.exit;


public class SqlBankPersistence implements Persistable<User, Integer> {

    String url = "jdbc:postgresql://database-dec17.cykqmxx6rmlt.us-east-2.rds.amazonaws.com:5432/bankapp";
    String username = "postgresjp";
    String password = "P3nnfam1ly!";


    public User getById(Integer integer) {
        return null;
    }


    public List<User> getAll() {
        return null;
    }


    public Integer save(User obj) {
        return null;
    }


    public void update(User obj) {

    }


    public void delete(User obj) {

    }

    public void insertInto() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the name of the table you want to insert into");
        String tableName = scan.next();
        if (tableName.equalsIgnoreCase("customer")) {
            System.out.println("Enter your customerid");
            double custid = scan.nextDouble();
            System.out.println("Enter your first name");
            String custFname = scan.next();
            System.out.println("Enter your last name");
            String custLname = scan.next();
            System.out.println("Enter your creditscore to insert");
            double creditScore = scan.nextDouble();
            System.out.println("Enter your acctnum to insert");
            double acctnum = scan.nextDouble();
            try {
                Connection conn = DriverManager.getConnection(url, username, password);
                String st = "INSERT INTO customer (customerid, fname, lname, creditscore, acctnum) VALUES ( ?, ?, ?, ?, ?);";
                PreparedStatement ps = conn.prepareStatement(st);
                //ps.setString 1 is pointing to the question mark above, a is pointing to the string you put in the variable and
                //swapping it with that question mark.
                ps.setDouble(1, custid);
                ps.setString(2, custFname);
                ps.setString(3, custLname);
                ps.setDouble(4, creditScore);
                ps.setDouble(5, acctnum);
                conn.setAutoCommit(true);
                //
                ps.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (tableName.equalsIgnoreCase("account")) {

            System.out.println("Enter your Account Number");
            double accntNum = scan.nextDouble();

            System.out.println("Is it an employee or customer account? C for customer, E for employee");
            String acctType = scan.next();


            System.out.println("Enter the balance of the account");
            double balanceAmt = scan.nextDouble();

            System.out.println("If it is a customer account enter the id associated with the customer. If not enter 0");

            double customerID = scan.nextDouble();
            if (customerID == 0) {
                System.out.println("Employee id. Exit");
                exit(0);
            }

            try {
                Connection conn = DriverManager.getConnection(url, username, password);
                String st = "INSERT INTO account (acctnum, accttype, balance, customerid) VALUES ( ?, ?, ?, ?)";
                PreparedStatement ps = conn.prepareStatement(st);
                //ps.setString 1 is pointing to the question mark above, a is pointing to the string you put in the variable and
                //swapping it with that question mark.
                ps.setDouble(1, accntNum);
                ps.setString(2, acctType);
                ps.setDouble(3, balanceAmt);
                ps.setDouble(4, customerID);
                ps.executeUpdate();
                conn.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void depositInto() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your account number!");
        int acctnum = scan.nextInt();
        System.out.println("Enter the amount you would like to deposit. Please enter numbers between the range of 1 and 10,000");
        double depositAmt = scan.nextDouble();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            String st = "Select balance from account where acctnum = ?";
            PreparedStatement ps = conn.prepareStatement(st);
            ps.setDouble(1, acctnum);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (depositAmt >= 1 && depositAmt <= 10000) {
                    double newBal = (rs.getDouble(1) + depositAmt);
                    String sqll = "UPDATE account SET balance = ? where acctnum = ?;";
                    PreparedStatement sql = conn.prepareStatement(sqll);
                    sql.setDouble(1, newBal);
                    sql.setDouble(2, acctnum);
                    sql.executeUpdate();
                    System.out.println("Your new balance after your deposit is:" + newBal);
                    conn.setAutoCommit(true);
                } else {
                    System.out.println("The input was invalid please try again");
                }

//
            }
//            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void withdrawOut() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your account number!");
        int acctnum = scan.nextInt();
        System.out.println("Enter the amount you would like to withdraw. It must be greater than 1 and less than your bank balance");
        double withdrawAmt = scan.nextDouble();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            String st = "Select balance from account where acctnum = ?";
            PreparedStatement ps = conn.prepareStatement(st);
            ps.setDouble(1, acctnum);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                // Check if balance is greater than withdraw amount and if withdraw amount is greater than zero
                if (((rs.getDouble(1) >= withdrawAmt) && (withdrawAmt >= 1))) {
                    double newBal = (rs.getDouble(1) - withdrawAmt);
                    String sqll = "UPDATE account SET balance = ? where acctnum = ?;";
                    PreparedStatement sql = conn.prepareStatement(sqll);
                    sql.setDouble(1, newBal);
                    sql.setDouble(2, acctnum);
                    sql.executeUpdate();
                    System.out.println("Your new balance after your withdraw is:" + newBal);
                    conn.setAutoCommit(true);
                } else {
                    System.out.println("Please make sure the amount you are trying to withdraw is valid.");
                }

//
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    //getAllCustomers
    public void viewAccounts() {
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            String st = "Select * FROM customer";
            Statement s = conn.createStatement();
            ResultSet re = s.executeQuery(st);
            while (re.next()) {
                ModelCustomer m = new ModelCustomer(re.getInt(1), re.getString(2), re.getString(3), re.getInt(4), re.getInt(5));
                System.out.println(m);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void viewIndividualBalance() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your account number!");
        int acctnum = scan.nextInt();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            String st = "Select balance from account where acctnum = ?";
            PreparedStatement ps = conn.prepareStatement(st);
            ps.setDouble(1, acctnum);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getDouble(1) >= 1) {
                    System.out.println("Your balance is " + rs.getDouble(1));
                    conn.setAutoCommit(true);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }


//
    }

    public void transferFunds() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your account number!");
        int acctnum = scan.nextInt();
        System.out.println("Enter the account number you would like to transfer too!");
        int acctnumToTrans = scan.nextInt();
        System.out.println("Enter the amount you would like to transfer. Please enter an amount between 1 and 10,000.");
        double transfAmt = scan.nextDouble();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            String st = "Select balance from account where acctnum = ?";
            PreparedStatement ps = conn.prepareStatement(st);
            ps.setDouble(1, acctnum);
            ResultSet rs = ps.executeQuery();
//            String stt = "Select balance from account where acctnum = ?";
//            PreparedStatement pss = conn.prepareStatement(stt);
            //1st check
//            pss.setDouble(1, acctnumTrans);
//            ResultSet rss = ps.executeQuery();
            while (rs.next()) {
                if (((rs.getDouble(1) > transfAmt) && (transfAmt >= 1) && (transfAmt <= 10000))) {
                    double minusBal = (rs.getDouble(1) - transfAmt);
                    String sttt = "UPDATE account SET balance = ? where acctnum = ?;";
                    PreparedStatement sql = conn.prepareStatement(sttt);
                    sql.setDouble(1, minusBal);
                    sql.setDouble(2, acctnum);
                    sql.executeUpdate();
                } else {
                    System.out.println("Sorry invalid input, please try again!");
                    break;
                }

                String stt = "Select balance from account where acctnum = ?";
                PreparedStatement pss = conn.prepareStatement(stt);
                pss.setDouble(1, acctnumToTrans);
                ResultSet rss = pss.executeQuery();
                while (rss.next()) {
                    String updAcctTransferedToo = "UPDATE account SET balance = ? where acctnum = ?;";
                    double newBalAftrTransf = (rss.getDouble(1) + transfAmt);
                    PreparedStatement sqll = conn.prepareStatement(updAcctTransferedToo);
                    sqll.setDouble(1, newBalAftrTransf);
                    sqll.setDouble(2, acctnumToTrans);
                    sqll.executeUpdate();
                    System.out.println("Your account balance after the transfer is:" + rs.getDouble(1));
                    System.out.println("The other account's balance after the transfer is:" + newBalAftrTransf);
//                     conn.setAutoCommit(true);

                }

            }


//

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void creditCheck() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Welcome to our automated loan approval system.");
        System.out.println("To begin, enter your account number!");
        int acctnum = scan.nextInt();
        try {
            Connection conn = DriverManager.getConnection(url, username, password);
            String st = "Select creditscore from customer where acctnum = ?";
            PreparedStatement ps = conn.prepareStatement(st);
            ps.setDouble(1, acctnum);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                double creditScoreAmt = rs.getDouble(1);
                if (creditScoreAmt < 579) {
                    System.out.println("I'm sorry, but we cannot approve you for a loan at this time");
                } else if ((creditScoreAmt >= 579) && (creditScoreAmt <= 669)) {
                    System.out.println("A representative will look at your account and decide to approve you for a loan. If approved it will be high interest");
                } else if ((creditScoreAmt >= 670) && (creditScoreAmt <= 739)) {
                    System.out.println("You have been approved for a loan! The interest rate will be decided at a future time");
                } else if ((creditScoreAmt >= 740) && (creditScoreAmt <= 850)) {
                    System.out.println("You have been approved for a loan! The interest rate will be low. Congrats");

                }
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public void validateLogin(String acctType, boolean isEmp) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your account number to login! Or if you are an employee enter your employee id");
        double enterNum = scan.nextDouble();
        if (acctType.equalsIgnoreCase("customer")) {
            acctType = "customer";


            if (isEmp == false) {
                try {
                    Connection conn = DriverManager.getConnection(url, username, password);
                    String st = "Select * FROM customer";
                    Statement s = conn.createStatement();
                    ResultSet re = s.executeQuery(st);
                    while (re.next()) {
                        ModelCustomer m = new ModelCustomer(re.getInt(1), re.getString(2), re.getString(3), re.getInt(4), re.getInt(5));
                        if ((m.getAcctNum() == enterNum)) {
                            System.out.println("Hello! You have successfully logged in as a customer. Thank you!");
                            Screen nextScreen = new EmployeeWelcomeScreen();
                            break;

                        } else if (((m.getAcctNum() != enterNum))) {
                            System.out.println("I'm sorry, but our system didn't recognize your input please try again.");
//                               exit(0);
                            break;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Sorry but the system has encountered a major error, please try again.");
                }
            }
        } else if (acctType.equalsIgnoreCase("employee")) {
            acctType = "employee";
            System.out.println("Enter your employee password");
            String pword = scan.next();


            if (isEmp == true) {
                try {
                    Connection conn = DriverManager.getConnection(url, username, password);
                    String st = "Select * FROM employee";
                    Statement s = conn.createStatement();
                    ResultSet re = s.executeQuery(st);
                    while (re.next()) {
                        ModelEmployee em = new ModelEmployee(re.getInt(1), re.getString(2), re.getString(3), re.getString(4));
                        if ((em.getId() == enterNum) && em.getPassWord().equals(pword)) {
                            System.out.println("Hello! You have successfully logged in as an employee. Thank you!");

                            break;

                        } else if (((em.getId() != enterNum) || ! em.getPassWord().equals(pword))) {
                            System.out.println("I'm sorry, but our system didn't recognize your input please try again.");
                            exit(0);
                            break;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Sorry, but our system has encountered a major error please try again.");
                }
            }


        } else System.out.println("Not a valid account type, exiting.");

    }
}