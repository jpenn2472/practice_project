package com.ex.app.models;

import java.util.ArrayList;

public class Users {
    ArrayList<User> users;

    public Users() {
        //users = new ArrayList<>();
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }
}
