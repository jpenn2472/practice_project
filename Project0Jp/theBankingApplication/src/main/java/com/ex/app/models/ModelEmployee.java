package com.ex.app.models;

public class ModelEmployee {

    private int employeeId;
    private String fName;
    private String lName;
    private String passWord;

    public ModelEmployee() {

    }
    public ModelEmployee(int employeeId, String fName, String lName, String passWord) {
        super();
        this.employeeId = employeeId;
        this.fName = fName;
        this.lName = lName;
        this.passWord = passWord;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getId() {
        return employeeId;
    }

    public void setId(int id) {
        this.employeeId = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    @Override
    public String toString() {
        return employeeId + " " + fName + " " + lName;
    }



}
