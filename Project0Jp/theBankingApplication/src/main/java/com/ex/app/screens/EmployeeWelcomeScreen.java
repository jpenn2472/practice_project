package com.ex.app.screens;
import com.ex.Application;
import com.ex.Screen;

import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.System.exit;

public class EmployeeWelcomeScreen implements Screen {
    public Screen doScreen(Scanner scanner, Application app) {
        System.out.println("--------" + app.getTitle() + "---------");
        System.out.println("Please make a selection:\n" +
                " 1. Open Account\n " +
                "2. View Accounts \n" +
                "3. View Customer Menu\n" +
                "4 Exit Program");

        Screen screen = new EmployeeWelcomeScreen();
        try {
            screen = doInput(scanner);
        } catch (InputMismatchException ex) {
            System.out.println("Input not recognized");
            scanner.next();
            screen = new EmployeeWelcomeScreen();
        }
        return screen;
    }

    private Screen doInput(Scanner scanner) {
        int i = scanner.nextInt();
        Screen nextScreen = null;

        switch(i) {
            case 1: nextScreen = new OpenAccountScreen();
                break;
            case 2: nextScreen = new ViewAccounts();
                break;
            case 3: nextScreen = new WelcomeScreen();
                break;
            case 4: exit(0);
                break;
            default:
        }
        return nextScreen;
    }
}
