package com.ex.app.screens;
import com.ex.Application;
import com.ex.Screen;

import java.util.InputMismatchException;
import java.util.Scanner;

import static java.lang.System.exit;

public class WelcomeScreen implements Screen {
    public Screen doScreen(Scanner scanner, Application app) {
        System.out.println("--------" + app.getTitle() + "---------");
        System.out.println("Please make a selection:\n" +
                " 1. Balance \n 2. Deposit\n 3. Transfer\n " +
                "4. Withdraw \n 5. Apply for Credit Loan \n 6. Login Screen\n" +
                "7 Exit Program\n");

        Screen screen;
        try {
            screen = doInput(scanner);
        } catch (InputMismatchException ex) {
            System.out.println("Input not recognized");
            scanner.next();
            screen = new WelcomeScreen();
        }
        return screen;
    }

    private Screen doInput(Scanner scanner) {
        int i = scanner.nextInt();
        Screen nextScreen = null;

        switch(i) {
            case 1:
                nextScreen = new BalanceScreen();
                break;
            case 2: nextScreen = new DepositScreen();
            break;
            case 3: nextScreen = new TransferScreen();
            break;
            case 4: nextScreen = new WithdrawScreen();
            break;
            case 5: nextScreen = new ApplyForCreditScreen();
            break;
            case 6: nextScreen = new LoginScreen();
            break;
            case 7: exit(0);
            default:
        }
        return nextScreen;
    }
}
