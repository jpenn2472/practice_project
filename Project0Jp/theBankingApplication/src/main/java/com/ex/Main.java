package com.ex;

import com.ex.app.system.BankApplication;

public class Main {

    public static void main(String[] args) throws Exception {
        Application app = new BankApplication("Bank");
        app.run(args);
    }

}
