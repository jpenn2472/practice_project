package com.ex;

import java.util.Scanner;

public interface Screen {
    Screen doScreen(Scanner scanner, Application app) throws Exception ;
}
