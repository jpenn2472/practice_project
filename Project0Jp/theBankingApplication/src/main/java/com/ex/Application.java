package com.ex;

import com.ex.app.persistence.SqlBankPersistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

import static java.lang.System.exit;

public abstract class Application {

    protected String title;

    public abstract void run(String[] args) throws Exception;

    public String getTitle() {

        return title;
    }



}
